import re


class PatternMatcher:
    def __init__(self, strings):
        self.patterns = [(s, self.generate_pattern(s)) for s in strings]

    @classmethod
    def generate_pattern(cls, string):
        return re.compile(string, re.IGNORECASE)

    def find_matches(self, message):
        matches = []
        for line in message.split("\n"):
            for string, pattern in self.patterns:
                m = pattern.search(line)
                if m is not None:
                    matches.append((string, m))
                    break
        return matches

    def format_matches_as_markdown(self, message):
        matches = self.find_matches(message)
        if not matches:
            return None
        return "\n".join(
            [PatternMatcher._format_match_as_markdown(m, s) for s, m in matches]
        )

    @classmethod
    def _format_match_as_markdown(cls, match, original_string):
        prefix = match.string[:match.start()]
        matched_text = match.string[match.start():match.end()]
        suffix = match.string[match.end():]

        result = f"{prefix}**{matched_text}**{suffix}"
        if original_string != matched_text:
            result += f" [{original_string}]"
        return result


class NameMatcher(PatternMatcher):
    @classmethod
    def generate_pattern(cls, full_name):
        name_parts = [part for part in full_name.split() if part != '']
        if not 2 <= len(name_parts) <= 3:
            raise ValueError(f"Could not parse name: '{full_name}'")
        last_name = name_parts[0]
        first_name = name_parts[1]
        return re.compile(
            f"(\\b{last_name}\\s+{first_name}\\b)|"
            f"(\\b{last_name}\\s+{first_name[0]}\\b)",
            re.IGNORECASE
        )
