from matching import PatternMatcher, NameMatcher


def test_names_pattern():
    full_name_pattern = NameMatcher.generate_pattern("Петров Иван Васильевич\n")
    assert full_name_pattern.search("петров и. в.")
    assert full_name_pattern.search("петров и")
    assert full_name_pattern.search("32. И.В. Петров") is None
    assert full_name_pattern.search("Днепропетров Иван") is None
    assert full_name_pattern.search("Петров Недоиван") is None

    short_name_pattern = NameMatcher.generate_pattern("Петрович Иван")
    assert short_name_pattern.search("41. Петрович И.В.")
    assert short_name_pattern.search("42. петрович и")
    assert short_name_pattern.search("петрович ольга ивановна") is None
    assert short_name_pattern.search("Руль Иван Петрович") is None
    assert short_name_pattern.search("Петровичев И. А.") is None
    assert short_name_pattern.search("Петрович В. С. и его друг") is None
    assert short_name_pattern.search("Фамилия и имя: Петрович Владислав") is None


def test_formatting():
    matcher = PatternMatcher(["needle", "another"])
    haystack = "Hay hay\nanother hay\nNo needle here\nand here\n"
    result = matcher.format_matches_as_markdown(haystack)
    assert result == "**another** hay\nNo **needle** here"


def test_name_formatting():
    matcher = NameMatcher(["Иванов Иван Иванович", "Иван Васильевич"])
    haystack = "Иванов И.И.\n1. Иван Васильевич\n"
    result = matcher.format_matches_as_markdown(haystack)
    assert result == (
        "**Иванов И**.И. [Иванов Иван Иванович]\n"
        "1. **Иван Васильевич**"
    )
