import logging
import datetime
import configparser
from telethon.sync import TelegramClient, events
from matching import NameMatcher


INIT_MESSAGES_HORIZON = 2000


class TelegramManager:
    def __init__(self, config, last_read_id):
        self.last_read_id = last_read_id
        self.on_new_message = lambda m: logging.warning(
            "Message handler is not set"
        )

        self.client = TelegramClient(
            config["username"], config["api_id"], config["api_hash"]
        )

        self.chat_name = config["chat_name"]

    def __enter__(self):
        self.client.__enter__()
        return self

    def __exit__(self, type, value, traceback):
        self.client.__exit__(type, value, traceback)

    def set_new_message_handler(self, on_new_message):
        self.on_new_message = on_new_message

    def run(self):
        logging.info("Processing unread messages")
        self.client.loop.run_until_complete(
            self.process_unread_messages(self.last_read_id)
        )

        logging.info("Listening for new messages")
        self.client.add_event_handler(
            self.handle_new_messages,
            events.NewMessage(chats=self.chat_name)
        )
        self.client.run_until_disconnected()


    async def process_unread_messages(self, last_read_id):
        chat = await self.client.get_entity(self.chat_name)

        if last_read_id is None:
            messages = await self.client.get_messages(
                chat, limit=INIT_MESSAGES_HORIZON
            )
            for message in messages[::-1]:
                await self.on_new_message(message)
        else:
            async for message in self.client.iter_messages(
                chat, reverse=True,
                offset_id=last_read_id
            ):
                await self.on_new_message(message)


    async def handle_new_messages(self, event):
        await self.on_new_message(event.message)

    async def send_message(self, chat_id, text):
        await self.client.send_message(chat_id, text)


class LatestMessageIdManager:
    def __init__(self, chat_name):
        self.filename = f"latest_read_message_id.{chat_name}"
        self.latest_id = None
        try:
            with open(self.filename, "r") as file:
                self.latest_id = int(file.read())
        except FileNotFoundError:
            pass
        except Exception as e:
            logging.warning("Could not read file %s", self.filename)
            raise

    def get_id(self):
        return self.latest_id

    def update_id(self, id):
        if self.latest_id is None or id > self.latest_id:
            self.latest_id = id
            with open(self.filename, "w") as file:
                file.write(str(id))


def link_to_message(chat_name, server_message_id):
    return f"https://t.me/{chat_name}/{server_message_id}"


def parse_config(filename):
    config = configparser.ConfigParser()
    config.read(filename)
    return config


def main():
    logging.getLogger().setLevel(logging.INFO)

    config = parse_config("config.ini")
    chat_name = config["Telegram"]["chat_name"]

    latest_id_manager = LatestMessageIdManager(chat_name)

    logging.info("Loading names database")
    name_matcher = None
    with open(config["Main"]["names_list"], "r") as f:
        lines = [line.strip() for line in f.readlines()]
        name_matcher = NameMatcher(lines)

    with TelegramManager(
        config["Telegram"],
        last_read_id=latest_id_manager.get_id()
    ) as tg:

        async def process_message(message):
            link = link_to_message(message.chat.username, message.id)
            print(link)

            if not message.message:
                logging.info("Message doesn't contain any text")
                return

            report_method = config["Main"]["report_method"]
            matches = name_matcher.format_matches_as_markdown(message.message)
            if report_method == "console":
                if matches is not None:
                    print(matches)
            elif report_method == "telegram":
                if matches is not None:
                    text = f"{link}\n{matches}"
                    await tg.send_message(int(config["Main"]["report_chat_id"]), text)
            else:
                logging.warn(f"Unknown report method: {report_method}")
            latest_id_manager.update_id(message.id)

        tg.set_new_message_handler(process_message)
        tg.run()


if __name__ == '__main__':
    main()
